# TFF2020 - Time-Frequency Fading

Code and data to reproduce experiments from paper
 *Time-frequency fading algorithms based on Gabor multipliers*
 by A. Marina Krémé, Valentin Emiya, Caroline Chaux and Bruno Torrésani, 2020.

The sound material is available in folder 'data'.

The code is available in folders 'matlab' and 'python'. The main experiments are available in both programming languages:

* Figure 1 and 2 can be reproduced in Matlab by running `tff2020/matlab/tfgm/scripts/exp_gabmul_eigs_properties.m`.
* Figure 3 can be reproduced in Matlab by running file `tff2020/matlab/tfgm/scripts/rank_estimation_halko_vs_eigs_gausswin.m`.
* Figure 4 can be reproduced in Python by running the specific tasks 12 and 13 from `tffpy.scripts.script_exp_solve_tff.py`.
* Figure 5 can be reproduced in Python by running the specific tasks 12 and 13 from `tffpy.scripts.script_exp_solve_tff.py`.
* Figure 6 can be reproduced in Python by running the full experiment from `tffpy.scripts.script_exp_solve_tff.py`.
* Table I can be reproduced in Python by running the full experiment from `tffpy.scripts.script_exp_solve_tff.py`.
* Table II can be reproduced in Python by running the full experiment from `tffpy.scripts.script_exp_solve_tff.py`.



