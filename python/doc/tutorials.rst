Tutorials and demonstrations
############################

.. toctree::
    :maxdepth: 1

    _notebooks/mask_energy_estimation.ipynb
    _notebooks/create_subregions.ipynb
    _notebooks/baseline_interpolation_solver.ipynb
