Credits
=======

Copyright(c) 2020
-----------------

* Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>
* Université d'Aix-Marseille <http://www.univ-amu.fr/>
* Centre National de la Recherche Scientifique <http://www.cnrs.fr/>
* Université de Toulon <http://www.univ-tln.fr/>

Contributors
------------

* Valentin Emiya <firstname.lastname_AT_lis-lab.fr>
* Ama Marina Kreme <firstname.lastname_AT_lis-lab.fr>

This package has been created thanks to the joint work with Florent Jaillet
and Ronan Hamon on other packages.

Licence
-------
This file is part of tffpy.

tffpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
